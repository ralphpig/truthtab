pub mod parser;

use parser::NonTerm::*;
use parser::Operator;
use parser::ParseTree;
use parser::ParseTree::*;
use parser::Parser;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;

pub struct Expr(ExprTree);

pub enum ExprTree {
    Uop(Operator, Box<Option<ExprTree>>),
    Bop(Operator, Box<Option<ExprTree>>, Box<Option<ExprTree>>),
    Id(String),
    Lit(bool),
}

impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Expr(tree) = self;

        write!(f, "{}", Expr::fmt(tree, false))
    }
}

// Public
impl Expr {
    pub fn new(expr: &str) -> Option<Expr> {
        let mut parser = Parser::new(&expr);
        if let Some(ref p_tree) = parser.parse() {
            if let Some(e_tree) = Expr::expr(p_tree) {
                return Some(Expr(e_tree));
            }
        }

        return None;
    }

    // pub fn print (&self) {
    //   let Expr(tree) = self;
    //   Expr::print_expr(tree);
    // }

    pub fn get_ids(&self) -> Vec<&str> {
        let Expr(tree) = self;
        let mut out: Vec<&str> = Expr::_get_ids(tree).into_iter().collect();
        out.sort();
        out
    }

    pub fn eval(&self, input: &HashMap<&str, bool>) -> bool {
        let Expr(tree) = self;
        Expr::_eval(tree, input)
    }
}

// Private
impl Expr {
    fn _eval(root: &ExprTree, input: &HashMap<&str, bool>) -> bool {
        use ExprTree::*;

        match root {
            Uop(ref op, ref _rhs) => match (op, _rhs.as_ref()) {
                (Operator::Not, Some(ref rhs)) => !(Expr::_eval(rhs, input)),
                _ => panic!("[Expr::_eval] Uop(Operator, _)"),
            },
            Bop(ref op, ref _lhs, ref _rhs) => match (_lhs.as_ref(), _rhs.as_ref()) {
                (Some(ref lhs), Some(ref rhs)) => match op {
                    Operator::Iff => !(Expr::_eval(lhs, input) ^ Expr::_eval(rhs, input)),
                    Operator::If => !(Expr::_eval(lhs, input) & !Expr::_eval(rhs, input)),
                    Operator::Or => Expr::_eval(lhs, input) | Expr::_eval(rhs, input),
                    Operator::Nor => !(Expr::_eval(lhs, input) | Expr::_eval(rhs, input)),
                    Operator::Xor => Expr::_eval(lhs, input) ^ Expr::_eval(rhs, input),
                    Operator::Nand => !(Expr::_eval(lhs, input) & Expr::_eval(rhs, input)),
                    Operator::And => Expr::_eval(lhs, input) & Expr::_eval(rhs, input),
                    e => panic!("[Expr::_eval] Bop Unexpected Operator {:?}", e),
                },
                _ => panic!("[Expr::_eval] Bop(_, None)"),
            },
            Id(ref id) => {
                if let Some(val) = input.get(id.as_str()) {
                    *val
                } else {
                    panic!("[Expr::_eval] No input for id={}", id);
                }
            }
            Lit(ref lit) => *lit,
        }
    }

    fn _get_ids(root: &ExprTree) -> HashSet<&str> {
        use ExprTree::*;

        match root {
            Uop(_, ref _rhs) => match _rhs.as_ref() {
                Some(ref rhs) => Expr::_get_ids(rhs),
                _ => panic!("[_get_ids] Uop(_, None)"),
            },
            Bop(_, ref _lhs, ref _rhs) => match (_lhs.as_ref(), _rhs.as_ref()) {
                (Some(ref lhs), Some(ref rhs)) => {
                    let mut out = Expr::_get_ids(lhs);
                    out.extend(&Expr::_get_ids(rhs));
                    out
                }
                _ => panic!("[_get_ids] Bop(_, None?, None?)"),
            },
            Id(ref v) => {
                let mut out: HashSet<&str> = HashSet::new();
                out.insert(v.as_str());
                out
            }
            Lit(_) => HashSet::new(),
        }
    }

    fn fmt(root: &ExprTree, bop_paren: bool) -> String {
        use ExprTree::*;

        match root {
            Uop(ref op, ref _rhs) => match _rhs.as_ref() {
                Some(ref rhs) => format!("{}({})", *op, Expr::fmt(rhs, true)),
                None => panic!("[print_expr] Uop(op, None) Unary op malformed"),
            },
            Bop(ref op, ref _lhs, ref _rhs) => match (_lhs.as_ref(), _rhs.as_ref()) {
                (Some(ref lhs), Some(ref rhs)) if bop_paren => format!(
                    "({} {} {})",
                    Expr::fmt(lhs, true),
                    *op,
                    Expr::fmt(rhs, true)
                ),
                (Some(ref lhs), Some(ref rhs)) if !bop_paren => {
                    format!("{} {} {}", Expr::fmt(lhs, true), *op, Expr::fmt(rhs, true))
                }
                _ => panic!("[print_expr] Bop(op, None?, None?) Binary op malformed"),
            },
            Id(v) => format!("{}", v),
            Lit(v) => format!("{}", v),
        }
    }

    fn expr(tree: &ParseTree) -> Option<ExprTree> {
        match tree {
            Node(Root, ref list)
            | Node(Fifth, ref list)
            | Node(Fourth, ref list)
            | Node(Third, ref list) => match list[..] {
                [ref _head, ref _tail] => match (_head.as_ref(), _tail.as_ref()) {
                    (Some(ref head), Some(ref tail)) => Expr::expr_tail(tail, Expr::expr(head)),
                    (Some(ref head), None) => Expr::expr(head),
                    _ => unreachable!(),
                },
                _ => panic!("[Expr::expr | Node(Root -> Third, _)] Invalid [ParseTree]"),
            },
            // First and FirstT are processed here because the context of FirstT must
            // be available during the parsing of First. This is because First contains
            // the Unary operator
            Node(Second, ref list) => match list[..] {
                [ref _first, ref _firstt] => match (_first.as_ref(), _firstt.as_ref()) {
                    (Some(Node(First, ref first_list)), Some(ref expr)) => match *first_list[0] {
                        Some(Op(ref op)) => {
                            Some(ExprTree::Uop(*op, Box::new(Expr::expr_tail(expr, None))))
                        }
                        None => Expr::expr_tail(expr, None),
                        _ => panic!("[Expr::expr | Node(First, [Some(Op)])] Invalid [ParseTree]"),
                    },
                    _ => panic!("[Expr::expr] Node(First, [None?])] Invalid [ParseTree]"),
                },
                _ => panic!("[Expr::expr | Node(Second, _)] Invalid [ParseTree]"),
            },

            e => panic!("[Expr::expr] Unexpected ParseTree {:?}", e),
        }
    }

    fn expr_tail(tree: &ParseTree, lhs: Option<ExprTree>) -> Option<ExprTree> {
        match tree {
            Node(FifthT, ref list)
            | Node(FourthT, ref list)
            | Node(ThirdT, ref list)
            | Node(SecondT, ref list) => match &list[..] {
              [ref _op, ref _head, ref _tail] => match (_op.as_ref(), _head.as_ref(), _tail.as_ref()) {
                (Some(Op(op)), Some(ref head), Some(ref tail))
                    => Some(ExprTree::Bop(*op, Box::new(lhs), Box::new(Expr::expr_tail(tail, Expr::expr(head))))),
                (Some(Op(op)), Some(ref head), None)
                    => Some(ExprTree::Bop(*op, Box::new(lhs), Box::new(Expr::expr(head)))),
                _ => panic!("[Expr::expr_tail | Node(FourthT -> SecondT, [op, head, tail])] Invalid [ParseTree]"),
              },
              [_] => None,
              _ => panic!("[Expr::expr_tail | Node(FourthT, _)] Invalid [ParseTree]"),
            },
            Node(FirstT, ref list) => match list[..] {
              [ref term] => match term.as_ref() {
                Some(Id(ref id)) => Some(ExprTree::Id(id.clone())),
                Some(Lit(lit)) => Some(ExprTree::Lit(*lit)),
                Some(ref expr @ Node(Root, _)) => Expr::expr(expr),
                e => panic!("[Expr::expr_tail | Node(FirstT, _)] Unexpected [ParseTree] item {:?}", e),
              },
              _ => panic!("[Expr::expr_tail | Node(FirstT, _)] Invalid [ParseTree]"),
            },
            e => panic!("[Expr::expr_tail] Unexpected ParseTree {:?}", e)
          }
    }
}
