pub mod expr;

use clap::{App, Arg};
use expr::Expr;
use prettytable::format::{FormatBuilder, LinePosition, LineSeparator};
use prettytable::{Cell, Row, Table};
use std::collections::HashMap;
use std::io;

const EXPR_HELP: &'static str = "() : scoping
------------
5 : = : iff
4 : > : if
3 : + : or
3 : - : nor
3 : ^ : xor
3 : | : nand
2 : & : and
1 : ~ : not";

fn main() {
    let args = App::new("Truth Table Generator")
        .version("1.0")
        .author("Mike Nelson <ralphpig@gmail.com>")
        .arg(
            Arg::with_name("pretty")
                .short("p")
                .long("pretty")
                .help("Use UTF8 table borders"),
        )
        .arg(
            Arg::with_name("repeat")
                .short("r")
                .long("repeat")
                .help("Repeat on stdin"),
        )
        .arg(
            Arg::with_name("expr")
                .short("e")
                .long("expr")
                .value_name("EXPR")
                .takes_value(true)
                .long_help(EXPR_HELP),
        )
        .get_matches();

    let mut input_expr: String;

    if let Some(expr) = args.value_of("expr") {
        input_expr = expr.to_string();
    } else {
        // Stdin
        input_expr = String::new();
        io::stdin().read_line(&mut input_expr).expect("Bad Input.");
    }

    loop {
        if let Some(expr) = Expr::new(&input_expr) {
            let mut table = Table::new();

            // Pretty Print
            if args.is_present("pretty") {
                let format = FormatBuilder::new()
                    .column_separator('│')
                    .borders('│')
                    .separators(
                        &[LinePosition::Top],
                        LineSeparator::new('─', '┬', '┌', '┐'),
                    )
                    .separators(
                        &[LinePosition::Intern],
                        LineSeparator::new('─', '┼', '├', '┤'),
                    )
                    .separators(
                        &[LinePosition::Bottom],
                        LineSeparator::new('─', '┴', '└', '┘'),
                    )
                    .padding(1, 1)
                    .build();
                table.set_format(format);
            }

            let ids = expr.get_ids();
            let ids_l = ids.len();

            // Generate the inputs for each row
            let mut rows: Vec<HashMap<&str, bool>> = Vec::new();
            for i in 0..2u32.pow(ids_l as u32) {
                let mut row_map: HashMap<&str, bool> = HashMap::new();
                let vals = (0..ids_l)
                    .map(|n| if ((i >> n) & 1) == 1 { true } else { false })
                    .rev();

                for (id, val) in ids.iter().zip(vals) {
                    row_map.insert(id, val);
                }
                rows.push(row_map);
            }

            let bool_items = |b: bool| if b { "1" } else { "0" };
            let style_spec = "cb";

            // Table headers
            table.add_row(Row::new(vec![
                Cell::new(
                    ids.iter()
                        .map(|id| format!(" {} ", id))
                        .collect::<String>()
                        .as_str(),
                )
                .style_spec(style_spec),
                Cell::new(&expr.to_string()).style_spec(style_spec),
            ]));

            // Build each row of table
            for ref row in &rows {
                table.add_row(Row::new(vec![
                    Cell::new(
                        ids.iter()
                            .map(|id| *row.get(id).unwrap())
                            .map(bool_items)
                            .map(|id| format!(" {} ", id))
                            .collect::<String>()
                            .as_str(),
                    )
                    .style_spec(style_spec),
                    Cell::new(bool_items(expr.eval(row))).style_spec(style_spec),
                ]));
            }

            // Print the table
            table.printstd();
        }

        // Loop option
        if args.is_present("repeat") {
            println!("");
            input_expr.drain(..);
            io::stdin().read_line(&mut input_expr).expect("Bad Input.");
        } else {
            break;
        }
    }
}
