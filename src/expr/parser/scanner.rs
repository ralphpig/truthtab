use std::fmt;
use std::iter::Iterator;
use std::iter::Peekable;
use std::str::Chars;

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Lparen,
    Rparen,
    Id(String),
    Lit(bool),
    Op(Operator),
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Operator {
    Iff,
    If,
    Or,
    Nor,
    Xor,
    Nand,
    And,
    Not,
}

impl fmt::Display for Operator {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Operator::*;
        let op = match self {
            Iff => '=',
            If => '>',
            Or => '+',
            Nor => '-',
            Xor => '^',
            Nand => '|',
            And => '&',
            Not => '~',
        };

        write!(f, "{}", op)
    }
}

pub struct Scanner<'a> {
    chars: Peekable<Chars<'a>>,
}

impl<'a> Scanner<'a> {
    pub fn new(expr: &'a str) -> Scanner {
        Scanner {
            chars: expr.chars().peekable(),
        }
    }
}

impl<'a> Iterator for Scanner<'a> {
    type Item = Token;
    fn next(&mut self) -> Option<Token> {
        use Token::*;

        let nxt;
        // Read whitespace until next char
        loop {
            match self.chars.next() {
                Some(c) if c.is_ascii_whitespace() => continue,
                Some(c) => {
                    nxt = c;
                    break;
                }
                None => return None,
            }
        }

        // Match token
        match nxt {
            '(' => Some(Lparen),
            ')' => Some(Rparen),
            '~' => Some(Op(Operator::Not)),
            '&' => Some(Op(Operator::And)),
            '+' => Some(Op(Operator::Or)),
            '-' => Some(Op(Operator::Nor)),
            '^' => Some(Op(Operator::Xor)),
            '|' => Some(Op(Operator::Nand)),
            '>' => Some(Op(Operator::If)),
            '=' => Some(Op(Operator::Iff)),
            d if d.is_digit(2) => Some(Lit(d == '1')),
            c if c.is_ascii_alphabetic() => {
                let mut id = c.to_string();
                // Build ID
                loop {
                    if let Some(nxt) = self.chars.peek() {
                        if !nxt.is_ascii_alphabetic() {
                            break;
                        }

                        id.push(*nxt);
                    } else {
                        break;
                    }

                    self.chars.next();
                }

                Some(Id(id))
            }
            e => panic!("Unexpected character {}", e),
        }
    }
}
