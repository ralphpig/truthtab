pub mod scanner;

pub use scanner::Operator;

use scanner::Operator::*;
use scanner::Scanner;
use scanner::Token;
use NonTerm::*;
use ParseTree::*;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum NonTerm {
    Root,
    Fifth,
    FifthT,
    Fourth,
    FourthT,
    Third,
    ThirdT,
    Second,
    SecondT,
    First,
    FirstT,
}

#[derive(Debug, Clone)]
pub enum ParseTree {
    Node(NonTerm, Vec<Box<Option<ParseTree>>>),
    Id(String),
    Lit(bool),
    Op(Operator),
}

pub struct Parser<'a> {
    curr: Option<Token>,
    scanner: Scanner<'a>,
}

// Public
impl<'a> Parser<'a> {
    pub fn new(expr: &'a str) -> Parser {
        Parser {
            curr: None,
            scanner: Scanner::new(expr),
        }
    }

    // pub fn print (root: &ParseTree, depth: usize) {
    //   let width = std::iter::repeat(" ").take(depth).collect::<String>();
    //   match root {
    //     Node(nt, list) => {
    //       println!("{}{:?}", width, nt);

    //       list.iter().for_each(|item| {
    //         if let Some(ref rhs) = **item { // deref ref + deref Box
    //           Parser::print(rhs, depth + 1)
    //         } else {
    //           println!("{} None", width);
    //         }
    //       });
    //     },
    //     Id(v) => println!("{}{}", width, v),
    //     Op(v) => println!("{}{:?}", width, v),
    //     Lit(v) => println!("{}{}", width, v),
    //   }

    //   if depth == 0 {
    //     println!("");
    //   }
    // }

    pub fn parse(&mut self) -> Option<ParseTree> {
        self.curr = self.scanner.next();
        self.root()
    }
}

// Private
impl<'a> Parser<'a> {
    fn expect(&mut self, etok: &Token) {
        match self.curr {
            Some(ref e) if *e != *etok => {
                panic!("[parse error] expected({:?}). found({:?}) ", etok, e)
            }
            None => panic!("[parse error] expected({:?}). found None.", etok),
            _ => self.next(),
        }
    }
    fn next(&mut self) {
        self.curr = self.scanner.next();
    }

    fn root(&mut self) -> Option<ParseTree> {
        let expr = match self.curr {
            Some(Token::Op(Not)) | Some(Token::Lparen) | Some(Token::Id(_))
            | Some(Token::Lit(_)) => vec![Box::new(self.fifth()), Box::new(self.fifth_tail())],

            Some(ref e) => panic!("Unexpected token: {:?}", e),
            None => return None,
        };

        Some(Node(Root, expr))
    }

    fn fifth(&mut self) -> Option<ParseTree> {
        let expr = match self.curr {
            Some(Token::Op(Not)) | Some(Token::Lparen) | Some(Token::Id(_))
            | Some(Token::Lit(_)) => vec![Box::new(self.fourth()), Box::new(self.fourth_tail())],

            // Error
            Some(ref e) => panic!("[fifth] Expected (~ | ( | id | lit). Found token: {:?}", e),
            None => panic!("[fifth] Unexpected end of input."),
        };

        Some(Node(Fifth, expr))
    }
    fn fifth_tail(&mut self) -> Option<ParseTree> {
        match self.curr {
            Some(Token::Op(Iff)) => {
                self.next();
                // self.expect(&Token::Op(Iff));
                Some(Node(
                    FifthT,
                    vec![
                        Box::new(Some(Op(Iff))),
                        Box::new(self.fifth()),
                        Box::new(self.fifth_tail()),
                    ],
                ))
            }

            // Follow
            None | Some(Token::Rparen) => None,

            // Error
            Some(ref e) => panic!("[fifth_tail] Expected EOF. Found token: {:?}", e),
        }
    }

    fn fourth(&mut self) -> Option<ParseTree> {
        let expr = match self.curr {
            Some(Token::Op(Not)) | Some(Token::Lparen) | Some(Token::Id(_))
            | Some(Token::Lit(_)) => vec![Box::new(self.third()), Box::new(self.third_tail())],

            // Error
            Some(ref e) => panic!("[fourth] Expected (~ | ( | id | lit). Found token: {:?}", e),
            None => panic!("[fourth] Unexpected end of input."),
        };

        Some(Node(Fourth, expr))
    }
    fn fourth_tail(&mut self) -> Option<ParseTree> {
        match self.curr {
            Some(Token::Op(If)) => {
                self.next();
                // self.expect(&Token::Op(If));
                Some(Node(
                    FourthT,
                    vec![
                        Box::new(Some(Op(If))),
                        Box::new(self.fourth()),
                        Box::new(self.fourth_tail()),
                    ],
                ))
            }

            // Follow
            None | Some(Token::Rparen) | Some(Token::Op(Iff)) => None,

            // Error
            Some(ref e) => panic!("[fourth_tail] Expected (> | =). Found token: {:?}", e),
            // None => panic!("[fourth_tail] Unexpected end of input.")
        }
    }

    fn third(&mut self) -> Option<ParseTree> {
        let expr = match self.curr {
            Some(Token::Op(Not)) | Some(Token::Lparen) | Some(Token::Id(_))
            | Some(Token::Lit(_)) => vec![Box::new(self.second()), Box::new(self.second_tail())],

            // Error
            Some(ref e) => panic!("[third] Expected (~ | ( | id | lit). Found token: {:?}", e),
            None => panic!("[third] Unexpected end of input."),
        };

        Some(Node(Third, expr))
    }
    fn third_tail(&mut self) -> Option<ParseTree> {
        match self.curr {
            Some(Token::Op(op @ Or))
            | Some(Token::Op(op @ Nor))
            | Some(Token::Op(op @ Xor))
            | Some(Token::Op(op @ Nand)) => {
                self.next();
                Some(Node(
                    ThirdT,
                    vec![
                        Box::new(Some(Op(op))),
                        Box::new(self.third()),
                        Box::new(self.third_tail()),
                    ],
                ))
            }

            // Follow
            None | Some(Token::Rparen) | Some(Token::Op(Iff)) | Some(Token::Op(If)) => None,

            // Error
            Some(ref e) => panic!(
                "[third_tail] Expected (+ | - | # | '|' | > | =). Found token: {:?}",
                e
            ),
            // None => panic!("[third_tail] Unexpected end of input.")
        }
    }

    fn second(&mut self) -> Option<ParseTree> {
        let expr = match self.curr {
            Some(Token::Op(Not)) | Some(Token::Lparen) | Some(Token::Id(_))
            | Some(Token::Lit(_)) => vec![Box::new(self.first()), Box::new(self.first_tail())],

            // Error
            Some(ref e) => panic!("[Second] Expected (~ | ( | id | lit). Found token: {:?}", e),
            None => panic!("[Second] Unexpected end of input."),
        };

        Some(Node(Second, expr))
    }
    fn second_tail(&mut self) -> Option<ParseTree> {
        match self.curr {
            Some(Token::Op(And)) => {
                self.next();
                // self.expect(&Token::Op(And));
                Some(Node(
                    SecondT,
                    vec![
                        Box::new(Some(Op(And))),
                        Box::new(self.second()),
                        Box::new(self.second_tail()),
                    ],
                ))
            }

            // Follow
            None
            | Some(Token::Rparen)
            | Some(Token::Op(Or))
            | Some(Token::Op(Nor))
            | Some(Token::Op(Xor))
            | Some(Token::Op(Nand))
            | Some(Token::Op(Iff))
            | Some(Token::Op(If)) => None,

            // Error
            Some(ref e) => panic!(
                "[second_tail] Expected (+ | - | # | '|' | > | =). Found token: {:?}",
                e
            ),
        }
    }

    fn first(&mut self) -> Option<ParseTree> {
        match self.curr {
            Some(Token::Op(Not)) => {
                self.next();
                Some(Node(First, vec![Box::new(Some(Op(Not)))]))
            }

            // Follow
            Some(Token::Lparen) | Some(Token::Id(_)) | Some(Token::Lit(_)) => {
                Some(Node(First, vec![Box::new(None)]))
            }

            // Error
            Some(ref e) => panic!(
                "[first] Expected (~ | '(' | id | lit). Found token: {:?}",
                e
            ),
            None => panic!("[first] Unexpected end of input."),
        }
    }
    fn first_tail(&mut self) -> Option<ParseTree> {
        let expr = match self.curr {
            Some(Token::Lparen) => {
                self.expect(&Token::Lparen);
                let out = self.root();
                self.expect(&Token::Rparen);

                Some(Node(FirstT, vec![Box::new(out)]))
            }
            Some(Token::Id(ref _id)) => {
                let out = Some(Node(FirstT, vec![Box::new(Some(Id(_id.clone())))]));
                self.next();
                out
            }
            Some(Token::Lit(lit)) => {
                self.next();
                Some(Node(FirstT, vec![Box::new(Some(Lit(lit)))]))
            }

            // Error
            Some(ref e) => panic!(
                "[first_tail] Expected ('(' | id | lit). Found token: {:?}",
                e
            ),
            None => panic!("[first_tail] Unexpected end of input."),
        };

        expr
    }
}
